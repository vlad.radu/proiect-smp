# Compilare ESP
Este nevoie de SDK-ul de la espressif (https://github.com/espressif/ESP8266_RTOS_SDK ) varianta de pe branch-ul master.

Se setaza variabila de mediu IDF_PATH la foldderul in care a fost clonat SDK-ul apoi se ruleaza:


    make flash monitor

# GUI
Este nevoie de urmatoarele dependinte: 

    matplotlib, pyserial

apoi se ruleaza in folderul /gui :

    python main.py

# Display
modificat de la https://github.com/derkst/ESP8266-OLED

# BMP
modificat de la https://github.com/UncleRus/esp-idf-lib/tree/master/components/bmp180
