#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "esp_system.h"
#include "esp_err.h"

#include "bmp/bmp180.c"
#include "oled/oled_graphics.c"

#include "driver/i2c.h"

static const char *TAG = "main";

#define I2C_EXAMPLE_MASTER_SCL_IO 5         /*!< gpio number for I2C master clock */
#define I2C_EXAMPLE_MASTER_SDA_IO 4         /*!< gpio number for I2C master data  */
#define I2C_EXAMPLE_MASTER_NUM I2C_NUM_0    /*!< I2C port number for master dev */



#define SSD1306_ADDR 0x3c

#define I2C_CLOCK 400000 // 400kHz

#define I2C_DELAY_US (1000000 / I2C_CLOCK) / 2

// i2c master init
static esp_err_t i2c_master_init()
{
    int i2c_master_port = I2C_EXAMPLE_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_EXAMPLE_MASTER_SDA_IO;
    conf.sda_pullup_en = 1;
    conf.scl_io_num = I2C_EXAMPLE_MASTER_SCL_IO;
    conf.scl_pullup_en = 1;
    conf.clk_stretch_tick = 300;
    ESP_ERROR_CHECK(i2c_driver_install(i2c_master_port, conf.mode));
    ESP_ERROR_CHECK(i2c_param_config(i2c_master_port, &conf));
    return ESP_OK;
}

static void smp_i2c_task(void *arg)
{
    vTaskDelay(100 / portTICK_RATE_MS);
    ESP_LOGI(TAG, "Before init");
    i2c_master_init();
    // initialize sensor
    bmp180_dev_t bmp_dev;
    bmp180_init_desc(&bmp_dev, I2C_EXAMPLE_MASTER_NUM, I2C_EXAMPLE_MASTER_SDA_IO, I2C_EXAMPLE_MASTER_SCL_IO);
    bmp180_init(&bmp_dev);

    //initialise display
    display_init(SSD1306_ADDR);

    while (1)
    {
        float temp;
        uint32_t pressure;
        bmp180_measure(&bmp_dev, &temp, &pressure, BMP180_MODE_HIGH_RESOLUTION);
        gfx_setTextColor(WHITE);
        gfx_setCursor(10, DISPLAYHEIGHT / 2 - 20);
        char temp_str[80];
        char press_str[80];
        sprintf(temp_str, "temp: %2f C", temp);
        sprintf(press_str, "pressure: %d Pa", pressure);
        ESP_LOGI("TX_SOFT", "temp: %2f | pres: %d | ", temp, pressure);
        display_clear();
        gfx_println(temp_str);
        gfx_setCursor(10, DISPLAYHEIGHT / 2);
        gfx_println(press_str);
        display_update();
        vTaskDelay(1000 / portTICK_RATE_MS);
    }

i2c_driver_delete(I2C_EXAMPLE_MASTER_NUM);
}

void app_main(void)
{
    //start i2c task
    xTaskCreate(smp_i2c_task, "smp_i2c_task", 2048, NULL, 10, NULL);
}