import threading
import serial
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import time
import random
import datetime as dt
import time
import re
import sys

connected = False
port = '/dev/ttyUSB0'
baud = 74880

DATA_TAG = 'TX_SOFT'

serial_port = serial.Serial(port, baud, timeout=100)

class Unit:
    def __init__(self, axis, color):
        self.axis = axis
        self.x = []
        self.y = []
        self.color = color
        axis.plot(self.x, self.y)

    def updatePlot(self, x, y):
        self.x.append(x)
        self.y.append(y)
        self.axis.plot(self.x, self.y, color=self.color)


def updateView():
    plt.draw()
    ax.relim()
    ax.autoscale_view()
    fig.canvas.flush_events()

def handle_data(data):
    tagIndex =  data.find(DATA_TAG)
    if ( tagIndex == -1):
        return
    
    now=time.mktime(time.localtime())
    timeDt = dt.datetime.fromtimestamp(now)
    
    tReg = re.search('temp: (.*?)\|\s', data)
    pReg = re.search('pres: (.*?)\|\s', data)
    tVal = tReg.group(1)
    pVal = pReg.group(1)
    
    print ("t: " + tVal)
    print ("p: " + pVal)

    temp.updatePlot(timeDt, float(tReg.group(1)))
    press.updatePlot(timeDt, int(pReg.group(1)))
    updateView()
     
def read_from_port(ser):
    connected = False
    while not connected:
        serin = ser.read()
        connected = True

        while True:
           reading = ser.readline().decode().strip()
           handle_data(reading)


fig, axs = plt.subplots(2)
fig.canvas.set_window_title('Monitorizare senzor')
temp = Unit(axs[0], "red")
press = Unit(axs[1], "blue")
axs[0].set_ylim([20, 35])
axs[0].set_title("Temperatura ( C )")
axs[1].set_ylim([50000, 60000])
axs[1].set_title("Presiunea atmosferica ( Pa )")
ax = plt.gca() 
xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
ax.xaxis.set_major_formatter(xfmt)
plt.gcf().autofmt_xdate()

thread = threading.Thread(target=read_from_port, args=(serial_port,))
thread.start()

plt.show()
plt.waitforbuttonpress(0) # this will wait for indefinite time
plt.close(fig)
sys.exit()  
